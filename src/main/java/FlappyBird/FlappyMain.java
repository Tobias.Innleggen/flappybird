package FlappyBird;

import javax.swing.*;

public class FlappyMain {

    public static void main(String[] args) {
        JPanel flappyMainPanel = new FlappyMainPanel();

        JFrame flappyFrame = new JFrame("FlappyBoi");
        flappyFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        flappyFrame.setResizable(true);

        flappyFrame.setContentPane(flappyMainPanel);

        flappyFrame.pack();
        flappyFrame.setVisible(true);
    }
}
