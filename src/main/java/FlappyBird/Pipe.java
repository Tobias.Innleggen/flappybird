package FlappyBird;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

public class Pipe extends JPanel {

    private final int pipeWidth;
    private int pipeHeight;
    private final int pipeX;
    private final int pipeInterval;
    private final int space;

    public Pipe(int screenWidth){
        Random ran = new Random();
        this.space = 300;
        this.pipeWidth =100;
        this.pipeHeight = 50 + ran.nextInt(300);
        this.pipeX = screenWidth;
        this.pipeInterval = 150;
    }

    /**
     *Adds rectangles with the desired dimensions and adds it to a list
     */
    public void addPipeLst(ArrayList<Rectangle> lst, int screenWidth, int screenHeight){
        lst.add(new Rectangle(screenWidth + pipeWidth + lst.size()*pipeInterval,screenHeight-pipeHeight-120,pipeWidth,pipeHeight));
        lst.add(new Rectangle(screenWidth + pipeWidth + (lst.size() - 1) * pipeInterval, 0, pipeWidth, screenHeight - pipeHeight - space));
    }

    public void drawPipe(Graphics g, Rectangle pipe){
        g.setColor(Color.GREEN.darker());
        g.fillRect(pipe.x, pipe.y, pipe.width, pipe.height);
    }

    public void movePipe(Rectangle pipe){
        pipe.x -= 5;
    }

    public int getX(){
        return this.pipeX;
    }

    public void setX(Rectangle pipe, int newX){
        pipe.x = newX;
    }

    //This method is made to make it easier to check if the bird can pass through pipes
    public void setPipeHeight(int screenHeight){
        this.pipeHeight = screenHeight/2-150;
    }

}
