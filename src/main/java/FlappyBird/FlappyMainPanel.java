package FlappyBird;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class FlappyMainPanel extends JPanel implements ActionListener, KeyListener {

    private final DrawBackground background = new DrawBackground();
    private final Bird bird = new Bird(background.getWidth(), background.getHeight());
    private final ArrayList<Rectangle> pipeLst = new ArrayList<>();
    private GameState gameState;

    private int score;

    public FlappyMainPanel(){
        gameState = GameState.START_GAME;
        this.setFocusable(true);
        this.setPreferredSize(new Dimension(background.getWidth(),background.getHeight()));
        Timer timer = new Timer(20,this);
        this.addKeyListener(this);
        timer.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        background.drawBackground(g);
        Pipe pipe = new Pipe(background.getWidth());
        pipe.addPipeLst(pipeLst,background.getWidth(),background.getHeight());

        for (Rectangle pipeFromLst : pipeLst) {
            pipe.drawPipe(g, pipeFromLst);
            if(gameState == GameState.ACTIVE_GAME) {
                pipe.movePipe(pipeFromLst);
            }
        }

        background.scoreCounter(g,score);

        if(gameState == GameState.START_GAME) {
            background.drawMessage(g,"Press space to start",Color.WHITE,0);
        } else if (gameState == GameState.GAME_OVER) {
            background.drawMessage(g,"GAME OVER",Color.RED,0);
            background.drawMessage(g,"PRESS SPACE TO RESTART",Color.RED,140);
        }

        bird.draw(g, bird.setRectangle());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(gameState == GameState.ACTIVE_GAME || gameState == GameState.GAME_OVER) {

            bird.gravity();

            for (Rectangle pipeFromLst : pipeLst) {
                //This is where it registers if the bird has passed through the pipes, I have not managed to perfect it.
                //It does not register the first pass through, and sometimes it just skips a pass through
                if (    pipeFromLst.y == 0
                        && bird.x + bird.width / 2 > pipeFromLst.x + pipeFromLst.width / 2
                        && bird.x + bird.width / 2 < pipeFromLst.x + pipeFromLst.width / 2 +10)
                {
                    score++;
                }
                else if (bird.setRectangle().y > background.getHeight() - 140) {
                    bird.setY(background.getHeight() - 140);
                    gameState = GameState.GAME_OVER;
                }
                else if (bird.setRectangle().y < 0) {
                    gameState = GameState.GAME_OVER;
                }
                else if (pipeFromLst.intersects(bird.setRectangle())) {
                    gameState = GameState.GAME_OVER;
                }
            }
        }
        this.repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            if (gameState == GameState.START_GAME) {
                gameState = GameState.ACTIVE_GAME;
            }
            else if (gameState == GameState.ACTIVE_GAME) {
                bird.flapp();
                this.repaint();
            }
            else if (gameState == GameState.GAME_OVER) {
                score = 0;
                pipeLst.clear();
                bird.setY(background.getHeight() / 2 - 10);
                bird.setSpeed(0);
                gameState = GameState.ACTIVE_GAME;
                }
            }
        }
    @Override
    public void keyReleased(KeyEvent e) {}
}