package FlappyBird;

import java.awt.*;

public class Bird extends Rectangle implements Iplayer{

    private final int startX;
    private double startY;
    private final int birdWidth;
    private final int birdHeight;

    private double speed = 0;

    public Bird(int frameWidth, int frameHeight){
        startX = frameWidth /2-10;
        startY = frameHeight /2-10;
        birdWidth = 20;
        birdHeight = 20;
    }

    @Override
    public Rectangle setRectangle(){
        return new Rectangle(startX,(int)startY,birdWidth,birdHeight);
    }

    @Override
    public void draw(Graphics g,Rectangle bird){
        g.setColor(Color.RED);
        g.fillRect(bird.x,bird.y,bird.width,bird.height);
    }

    @Override
    public void gravity(){
        double gravity = 0.58;
        double terminalVelocity = 50;
        this.speed += gravity;
        if(this.speed > terminalVelocity){
            this.speed = terminalVelocity;
        }
        this.startY+=this.speed;
    }

    public void flapp(){
        this.speed = -5;
        startY -= 75 ;
    }

    @Override
    public void setY(int newY){
        startY = newY;
    }

    @Override
    public double getBirdY(){
        return this.startY;
    }

    public void setSpeed(int newSpeed){
        this.speed = newSpeed;
    }

}

