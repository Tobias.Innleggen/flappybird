package FlappyBird;

import java.awt.*;

interface Iplayer {

    /**
     * Sets the bounds of the player object
     * @return a rectangle with the desired dimensions of the player
     */
    Rectangle setRectangle();

    /**
     * @param g defines where to draw
     * @param bird where it gets the dimensions of the object that is to be drawn
     */
    void draw(Graphics g,Rectangle bird);

    /**
     * Made to simulate a falling motion
     */
    void gravity();

    /**
     * @param newY the player object's y value is set to this value
     */
    void setY(int newY);

    /**
     * @return the current y value for bird
     */
    double getBirdY();

}
