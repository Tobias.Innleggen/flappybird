package FlappyBird;

public enum GameState {
    START_GAME,
    ACTIVE_GAME,
    GAME_OVER
}
