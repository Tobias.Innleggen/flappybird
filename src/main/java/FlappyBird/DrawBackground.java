package FlappyBird;

import javax.swing.*;
import java.awt.*;

public class DrawBackground extends JPanel {

    private final int frameWidth;
    private final int frameHeight;

    public DrawBackground() {
        this.frameWidth = 500;
        this.frameHeight = 800;
    }

    public void drawBackground(Graphics g) {
        g.setColor(Color.CYAN);
        g.fillRect(0, 0, frameWidth, frameHeight);

        g.setColor(Color.orange);
        g.fillRect(0, frameHeight - 120, frameWidth, 150);

        g.setColor(Color.GREEN);
        g.fillRect(0, frameHeight - 120, frameWidth, 20);
    }

    public void scoreCounter(Graphics g, int score) {
        g.setColor(Color.WHITE);
        g.setFont(new Font("Times new roman", Font.BOLD, 100));
        g.drawString(String.valueOf(score), this.getWidth() / 2 - 25, this.getHeight() - 600);
    }

    /**
     *draws the message on the screen, the offset is used to adjust the position of the text
     */
    public void drawMessage(Graphics g, String message, Color textColor, int offset) {
        int textWidth = g.getFontMetrics().stringWidth(message);
        g.setColor(textColor);
        g.setFont(new Font("Times new roman", Font.BOLD, 30));
        g.drawString(message, this.getWidth() / 2 - textWidth / 6-offset, this.getHeight() / 2 - 100+offset/4);
    }

    public int getWidth(){
        return this.frameWidth;
    }

    public int getHeight(){
        return this.frameHeight;
    }
}
