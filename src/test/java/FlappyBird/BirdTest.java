package FlappyBird;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Objects;

import org.junit.jupiter.api.Test;


public class BirdTest {

    int frameWidth = 500;
    int frameHeight = 800;

    @Test
    void BirdFlappTest() {
        Bird bird = new Bird(frameWidth, frameHeight);
        bird.flapp();
        assertEquals(315,bird.getBirdY());
    }

    @Test
    void doubleFlappTest(){
        Bird bird = new Bird(frameWidth,frameHeight);
        bird.flapp();
        bird.flapp();
        assertEquals(240,bird.getBirdY());
    }

    @Test
    void gravityTest(){
        Bird bird = new Bird(frameWidth,frameHeight);
        bird.gravity();
        assertEquals( 390+0.58,bird.getBirdY());
    }
}
