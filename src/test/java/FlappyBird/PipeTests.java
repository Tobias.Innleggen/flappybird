package FlappyBird;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.awt.*;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class PipeTests {

    int frameWidth = 500;
    int frameHeight = 800;
    /**
     * This test checks if the right amount of pipes ar added to the list
     */
    @Test
    void addPipeTest(){
        Pipe pipe = new Pipe(frameWidth);
        ArrayList<Rectangle> testLst = new ArrayList<>();
        pipe.addPipeLst(testLst,frameWidth,frameHeight);
        assertEquals(2,testLst.size());
        pipe.addPipeLst(testLst,frameWidth,frameHeight);
        pipe.addPipeLst(testLst,frameWidth,frameHeight);
        pipe.addPipeLst(testLst,frameWidth,frameHeight);
        assertEquals(8,testLst.size());
    }

    @Test
    void movePipeTest(){
        Pipe pipe = new Pipe(frameWidth);
        ArrayList<Rectangle> testLst = new ArrayList<>();
        pipe.addPipeLst(testLst,frameWidth,frameHeight);
        pipe.movePipe(testLst.get(0));
        pipe.movePipe(testLst.get(1));
        assertEquals(595,testLst.get(0).x);
        assertEquals(595,testLst.get(1).x);
    }


}

