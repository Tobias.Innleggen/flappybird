package FlappyBird;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Objects;

import org.junit.jupiter.api.Test;

public class BackGroundTets {

    @Test
    void getWidthTest(){
        DrawBackground testBackground = new DrawBackground();
        assertEquals(500,testBackground.getWidth());
    }

    @Test
    void getHeightTest(){
        DrawBackground testBackground = new DrawBackground();
        assertEquals(800,testBackground.getHeight());
    }
}
