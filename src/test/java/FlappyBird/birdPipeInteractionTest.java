package FlappyBird;

import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class birdPipeInteractionTest {

    int frameWidth = 500;
    int frameHeight = 800;

    /**
     * This test checks if the game can register if the pipe and bird collides
     * I set the bird y first to 0 cause there will always be a pipe in that height
     * Then I set the pipe x to the same as the bird x to see if the intersects method works
     */
    @Test
    void collisionTest(){
        Bird bird = new Bird(frameWidth,frameHeight);
        Pipe pipe = new Pipe(frameWidth);
        bird.setY(0);
        ArrayList<Rectangle> testLst = new ArrayList<>();
        pipe.addPipeLst(testLst,frameWidth,frameHeight);
        pipe.setX(testLst.get(1),bird.setRectangle().x);
        assertTrue(bird.setRectangle().intersects(testLst.get(1)));
        bird.setY(frameHeight-140);
        pipe.setX(testLst.get(0),bird.setRectangle().x);
        assertTrue(bird.setRectangle().intersects(testLst.get(0)));
    }

}
