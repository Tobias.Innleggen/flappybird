Plan:

Need to draw background first 
Draw the bird on top of background
Draw pipe in middle of screen
Move both pipe and bird
The plan is to move the bird just along the y-axis and the pipes along th x-axis
Generate more pipes with a given interval
Register collision between pipe and bird
Get a score counter

How to play:

The point of flappybird is to get through the space between the oncoming pipes
Your score will increase every time you pass through a set of pipes
The game is over if you hit one of the pipes, the ground or the ceiling

Code explanation:

The way I have coded this is that I make list of rectangles. The reason I use rectangles is that they have a method
called intersect. This method makes checking for collision between the bird and the pipes a lot easier. 
When drawing the pipes I iterate through the list and draw them in the paintComponent method. 
When checking for collision I also iterate through the list of pipes but instead of using the dimensions for drawing I
use them to check if it intersects with the bird. With this iterator I also check if the bird has passed through the pipes
this proved a lot harder than expected. It still does not register the first time it passes through a pipe and sometimes
it just skips a point. 

I tried to include the demo video in the git repository but the fle was too big for codegrade.
Therefore, I made the video available through this link https://youtu.be/gWv2W9THcyI
I have also included the class diagram and demo video in this shared disk folder
https://drive.google.com/drive/folders/1n1HKtWhX_NiCSgBBw9UnGr9m_sk_aVGd?usp=sharing